**Libraries**
- [JSON](#json)
- [Annotation-Processor](#annotation-processor)
- [Dependency Injection](#dependency-injection)
- [Inject in Tests](#inject-in-tests)
- [Jakarta EE Testing](#jakarta-ee-testing)
- [SimpleArchitecture](#simplearchitecture)
- [Nano App](#nano-app)

# Libraries
## JSON
### Dependency
```xml
<dependency>
    <groupId>com.joofthan.enterprise</groupId>
    <artifactId>json</artifactId>
    <version>1.0.0</version>
</dependency>

```
### Useage
With this call:
```Java
JSON json = new JSON()
                .with("one", 1.2)
                .with("jason", new JSON().with("age": 21).with("name", "burn"));
```
json.content will contain the following JSON:
 ```JSON
{
    "one": 1.2,
    "jason": {
        "age": 21,
        "name": "burn"
    }
}
```
The following Types are supported as value:
- boolean = true/false
- int = 0
- double = 0.0
- String = ""
- JSON = {}
- List<String> = []

## Annotation-Processor
### Dependency
```xml
<dependency>
    <groupId>com.joofthan.enterprise</groupId>
    <artifactId>annotation-processor</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```
### Plugin
```xml
<plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.6.1</version>
        <configuration>
            <showWarnings>true</showWarnings>
            <annotationProcessors>
                <annotationProcessor>
                    com.joofthan.enterprise.annotation.CollectClassesProcessor
                </annotationProcessor>
            </annotationProcessors>
        </configuration>
</plugin>
```
### Useage
##### CollectClasses
Create a Custom Annotation, which is annotated with @CollectClasses. And with the parameters: className="RegelFinder" and methodName="findAllRegeln"
```Java
@Target(value={TYPE, METHOD})
@Retention(RetentionPolicy.RUNTIME)
@CollectClasses(className = "RegelFinder", methodName = "findAllRegeln")
public @interface MyAnnotation {}
```
The following call will return all Classes that are annotated with @MyAnnotation:
```Java
List<Class> annotatedClasses =  RegelFinder.findAllRegeln();
```

## Dependency Injection
### Dependency
```xml
        <!--
        You can download and install CDI from here:
        https://gitlab.com/joofthan/infra/maven-poms#dependency-injection
        -->
        <dependency>
            <groupId>com.joofthan.enterprise</groupId>
            <artifactId>cdi-api</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>com.joofthan.cdi</groupId>
            <artifactId>cdi-implementation</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>
```
### Install
Clone this Repository and run `mvn install` on the root pom.
### Useage
#### Example
With CDI you can register implementations for interfaces and then construct big dependency trees without annotating classes.
Here is an example class MyService which requires an instance of interface "DataRepo" and an instance of class "JSONParser".
```Java
class MyService{
        DataRepo repo;
        JSONParser parser;
        MyService(DataRepo repo, JSONParser parser) {
            this.repo = repo;
            this.parser = parser;
        }
}
```
```Java
class MyRepo implements DataRepo{
    ...
}
```
```Java
class JSONParser {
    ...
}
```
We have to register which implementation should be used for the interface "DataRepo", because the interface could be implemented by multiple classes. For classes the behaviour is different. CDI will automatically search for the constructor and figure out all required dependencies. 
Then it will try to instanciate these using the NoArgsConstructor. If there is only one constructor with parameters, it constructs the dependencies of the constructor.
At the end of this dependency tree has to be a class with NoArgsConstructor or an class/interface which was registered using the register method.
So that CDI can successfully build the object tree.
```Java
CDI cdi = new CDIImpl();
cdi.register(DataRepo.class, new MyRepo("data.txt"));
var service = cdi.inject(MyService.class);
```
The call of _cdi.inject(MyService.class)_  will construct the following object tree in memory:

MyService
- MyRepo
- JSONParser

If the JSONParser would have additional dependencies, CDI would instanciate and provide these before injecting JSONParser into MyService.
#### Scopes
If classes are anotated with @ApplicationScoped, cdi will provide the same instance every time a class requests an instance of that type.
If the class is annotated with @RequestScoped, cdi will always construct a new Object for injecting into the constructor.
@RequestScoped is the default behaviour if there are no annotations.
# Jakarta EE Testing
## Dependency
```xml
        <!--
        You can download and install Jakarta EE Testing from here:
        https://gitlab.com/joofthan/infra/maven-poms#jakarta-ee-testing
        -->
        <dependency>
            <groupId>com.joofthan.enterprise</groupId>
            <artifactId>jakarta-9-testing</artifactId>
            <version>1.0.0</version>
            <scope>test</scope>
        </dependency>
```
### Useage
- Mockito
- jakarta-9-inject

See: [Inject in Tests](#inject-in-tests)
# Inject in Tests

## Dependency (javax)
```xml
        <dependency>
            <groupId>com.joofthan.enterprise</groupId>
            <artifactId>javax-inject</artifactId> <!-- alternative: jakarta-9-inject (no Mockito) -->
            <version>1.0.0</version>
            <scope>test</scope>
        </dependency>
```

## Useage
```Java
@EnableInject
class UserServiceTest {
    
    @Inject
    UserService userService;
    
    @UseForInject
    IUserRepo repo = new UserRepoMock();
    
    @Test
    void userIsPresent() {
        assertNotNull(userService.getUser());
    }
}
```
Add `@EnableInject` on a Junit Test class to use `@Inject` on Fields. It will simply set all annotated fields and subfields with an instance of the type.

If a field in the test class is annotated with `@UseForInject` this instance will be used instead everywhere where the fields type is injected.

# SimpleArchitecture
## Dependency
```xml
        <!--
        You can download and install SimpleArchitecture from here:
        https://gitlab.com/joofthan/infra/maven-poms#simplearchitecture
        -->
        <dependency>
            <groupId>com.joofthan.enterprise</groupId>
            <artifactId>simple-architecture</artifactId>
            <version>0.0.1-SNAPSHOT</version>
            <scope>test</scope>
        </dependency>
```
## Useage
```Java
class ArchitectureTest {
    @Test
    void testArchitecture() {
        SimpleArchitecture.check("com.example");
    }
}
```
Matching rules: [Archunit Documentation](https://www.javadoc.io/doc/com.tngtech.archunit/archunit/latest/com/tngtech/archunit/base/PackageMatcher.html)

SimpleArchitecture performs simple checks on the package structure to enforce loosely coupled packages.
Packages can only access the next lower subpackage.

In the following package structure:

- mypackage.module_a
- mypackage.module_a.submodule1
- mypackage.module_a.submodule1.subsubpackage
- mypackage.module_a.submodule2
- mypackage.module_b

ModuleA can access Submodule1 and Submodule2 but not subsubpackage.
subsubpackage can only be accessed from Submodule1.

Every Module consists of 2 packages:
- module1.api : _contains all public Interfaces and DataTransferObjects_
- module1 : _contains all private classes that implement the api_

### Example:
- mypackage.module_a
- mypackage.module_a.api
- mypackage.module_a.submodule1
- mypackage.module_a.submodule1.api
- mypackage.module_b.api 
- //_ModuleB Implementation in other jar_

# Nano App
## Dependency
```xml
        <dependency>
            <groupId>com.joofthan.enterprise</groupId>
            <artifactId>application-api</artifactId>
            <version>0.1.0-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>com.joofthan.nano</groupId>
            <artifactId>application-nano</artifactId>
            <version>0.1.0-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>com.joofthan.webserver</groupId>
            <artifactId>embedded-webserver</artifactId>
            <version>0.1.0-SNAPSHOT</version>
        </dependency>
```
## Useage
Hello World App:
```Java
    public static void main(String[] args) {
        var server = new EmbeddedWebServer(8080);
        var nano = new NanoApp();
        server.run(request->{
            RequestApp app = nano.process(request);
            app.get("/", ()->"Hello World");
            app.post("/", (body)->"Recieved data: "+body);
            return app.getResponse();
        });
    }
```
## Modules
You can split your application into multiple modules.
### Sub Module
Create a module by implementing RequestListener:
```Java
public class LoginModule implements RequestListener{
    public void process(RequestApp app) {
            app.post("/login", (data)->"login successful: " + data);
            app.get("/logout", ()->"logout successful");
    }
}
```
Register your module using the executeModule method: 
```Java
   app.executeModule("/user", new LoginModule());
```
The module will be activated if the request path starts with "/user".

Available paths from LoginModule:
- /user/login
- /user/logout

### Main Module
You can also register modules as main modul. Main modules do not have a sub path:
```Java
   app.executeModule(new LoginModule());
```

Available paths from LoginModule:
- /login
- /logout

## Plugins
After instanciating the NanoApp you can register plugins of the following types.

Interfaces:
- RequestListener 
- ResponseConverter

```Java
       App nano = new NanoApp();
       nano.addPlugin(Object.class, new JsonConverter());
       nano.addPlugin(new LoginModule());
```
RequestListener will be called every time a request is processed. They can be used to add routes or validate/authorise the request.
ResponseConverter will be called if the registered type or a subtype was returned by a route. 

For example the route: _app.get("/", ()-> new Customer())_ will call the JSONConverter to convert the customer to JSON because the class Customer extends from Object.
```Java
class JsonConverter implements ResponseConverter{
    Gson gson = new Gson();
    Response convert(Object object) {
        return new Response(gson.toJson(object), ContentType.JSON);
    }
}
```
You can register multiple converters for different types to return HTML, JSON, or XML depending on the type of the Object.