package com.example;

import com.example.servlet.MyServlet;
import com.example.converter.JsonConverter;
import com.example.converter.CustomerConverter;
import com.example.app.Customer;
import com.google.gson.Gson;
import com.joofthan.enterprise.application.Request;
import com.joofthan.enterprise.application.RequestType;
import com.joofthan.enterprise.application.Response;
import com.joofthan.nano.NanoApp;
import com.joofthan.webserver.EmbeddedWebServer;
import java.io.IOException;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

public class Main {

    public static void main(String[] args) throws Exception {
        //runConsoleExample();
        runEmbeddedExample();
        //runServletExample();
        
        //GlassFish glassfish = GlassFishRuntime.bootstrap().newGlassFish();
        //glassfish.start();
    }

    private static void runConsoleExample() {
        var nano = new NanoApp();
        nano.addPlugin(Object.class, new JsonConverter());
        nano.addPlugin(Customer.class, new CustomerConverter());
        nano.addPlugin(new MainModule());

        var app = nano.process(new Request("/hi", "requestBody", RequestType.GET));
        System.out.println(app.getResponse());
    }

    private static void runEmbeddedExample() {
        var server = new EmbeddedWebServer(8080);
        var nano = new NanoApp();
        nano.addPlugin(Object.class, new JsonConverter());
        nano.addPlugin(Customer.class, new CustomerConverter());
        nano.addPlugin(new MainModule());
        server.run((req) -> nano.process(req).getResponse());
    }

    private static void runServletExample() throws IOException, LifecycleException {
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8081);
        tomcat.addServlet("/", "myServ", new MyServlet());
        tomcat.start();
        System.in.read();
        System.out.println("Exited");
    }
}
