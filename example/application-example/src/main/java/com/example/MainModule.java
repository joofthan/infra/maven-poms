

package com.example;

import com.example.route.IndexModule;
import com.example.route.customer.CustomerModule;
import com.joofthan.enterprise.application.RequestApp;
import com.joofthan.enterprise.application.RequestListener;

public class MainModule implements RequestListener{

    private CustomerModule customerModule = new CustomerModule();
     private IndexModule indexModule = new IndexModule();
     
    @Override
    public void process(RequestApp app) {
        app.executeModule(indexModule);
        app.executeModule("/index", indexModule);
        app.executeModule("/index2", new IndexModule());
        app.executeModule("/customer", customerModule);
    }
}
