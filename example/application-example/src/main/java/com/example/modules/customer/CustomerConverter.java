

package com.example.converter;

import com.example.app.Customer;
import com.google.gson.Gson;
import com.joofthan.enterprise.application.Response;
import com.joofthan.enterprise.application.ResponseConverter;

public class CustomerConverter implements ResponseConverter {

    @Override
    public Response convert(Object object) {
        if(object.getClass().isAssignableFrom(Customer.class)){
            final Customer cusomer = (Customer)object;
            return Response.text(cusomer.name +" "+cusomer.alter );
      }else{
            return Response.json(object.getClass().getSimpleName()+" is sub class of Customer.  JSON Value: " + new Gson().toJson(object));
        }
    }

}
