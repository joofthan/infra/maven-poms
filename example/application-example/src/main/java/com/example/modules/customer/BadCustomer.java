

package com.example.app;

import java.math.BigDecimal;

public class BadCustomer extends Customer{

    public BigDecimal angryLevel;
    
    public BadCustomer(String name, int alter, double angryLevel) {
        super(name, alter);
        this.angryLevel = BigDecimal.valueOf(angryLevel);
    }
    

}
