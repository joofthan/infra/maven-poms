

package com.example.route.customer;

import com.example.app.BadCustomer;
import com.example.app.Customer;
import com.joofthan.enterprise.application.RequestApp;
import com.joofthan.enterprise.application.RequestListener;

public class CustomerModule implements RequestListener{

    @Override
    public void process(RequestApp app) {
        app.get("/", ()-> "Try: \n/cusomer/max\n/customer/bad");
        app.get("/max", ()-> new Customer("Max", 12));
        app.get("/bad", ()-> new BadCustomer("Badman", 50, 99.9));
    }

}
