package com.example.route;

import com.joofthan.enterprise.application.RequestApp;
import com.joofthan.enterprise.application.RequestListener;

public class IndexModule implements RequestListener {

        private int integerRepository;

    @Override
    public void process(RequestApp app) {
        app.get("/", () -> "Hello Request number " + getRequestNumber()+".\n\nTry\n/hi\n/customer\n/index1\n/index2");
        app.get("/hi", () -> "Hello");
    }

    private int getRequestNumber() {
        return ++integerRepository;
    }
}
