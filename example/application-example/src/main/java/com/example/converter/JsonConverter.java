

package com.example.converter;

import com.google.gson.Gson;
import com.joofthan.enterprise.application.ContentType;
import com.joofthan.enterprise.application.Response;
import com.joofthan.enterprise.application.ResponseConverter;

public class JsonConverter implements ResponseConverter{
    private final Gson gson = new Gson();
    
    @Override
    public Response convert(Object object) {
        return new Response(gson.toJson(object), ContentType.JSON);
    }
    
}
