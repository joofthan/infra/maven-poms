

package com.example.servlet;

import com.example.app.Customer;
import com.example.app.Customer;
import com.example.MainModule;
import com.example.converter.CustomerConverter;
import com.example.converter.CustomerConverter;
import com.example.converter.JsonConverter;
import com.example.converter.JsonConverter;
import com.example.MainModule;
import com.joofthan.enterprise.application.App;
import com.joofthan.enterprise.application.Request;
import com.joofthan.enterprise.application.RequestType;
import com.joofthan.nano.NanoApp;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MyServlet extends HttpServlet{

     private App nano = new NanoApp();

    @Override
    public void init() throws ServletException {
        nano.addPlugin(Object.class,  new JsonConverter());
        nano.addPlugin(Customer.class,  new CustomerConverter());
        nano.addPlugin(new MainModule());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var body = new String(req.getInputStream().readAllBytes());
        var response = nano.process(new Request(req.getContextPath(), body , RequestType.GET)).getResponse();
        resp.setHeader("contentType", response.contentType().mimeType());
        resp.getWriter().write(response.body());
    }
    
}
