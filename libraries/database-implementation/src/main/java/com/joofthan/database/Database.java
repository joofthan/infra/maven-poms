package com.joofthan.database;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.joofthan.enterprise.database.IDatabase;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class Database implements IDatabase{
        private String key;
        private Map<String, Object> cache = new HashMap<>();
        private Map<Object, String> ids = new HashMap<>();
        private ObjectMapper objectMapper = new ObjectMapper();
        private Logger log = Logger.getLogger(Database.class.getName());
        
        public Database(String key) {
            this.key=key;
        }

        public <T> T load(Class<T> clazz, Long qualifier){
            if(qualifier == null){
                return load(clazz);
            }else {
                return load(clazz, qualifier.toString());
            }
         }
        
         public <T> T load(Class<T> clazz, String id){
             log.info("Loading: "+clazz.getSimpleName() + " from: "+id);
              try {
                final File file = buildPath(clazz, id).toFile();
                if(!file.exists()){
                    return null;
                }else if(cache.containsKey(id)){
                    return (T) cache.get(id);
                }
                T entity = toEntity(file, clazz, id);
                cache.put(id, entity);
                cache.put(entity.toString(), entity);
                 ids.put(entity, id);
                 final String newId = getId(entity);
                 if(newId == null){
                     throw new RuntimeException("id is null");
                 }
                return entity;
            }  catch (Exception  ex) {
               throw new RuntimeException(ex);
            } 
         }

    private <T> Path buildPath(Class<T> clazz, String qualifier) {
        return Path.of(key, clazz.getName(), qualifier+".json");
    }

    private <T> T toEntity(final File fileReader, Class<T> clazz, String id) throws Exception {
        Constructor<T> constructor;
        try{
            constructor = clazz.getConstructor();
        }catch(NoSuchMethodException e){
             throw new RuntimeException("NoArgsConstructor required on: "+clazz.getName());
        }
        T entity = constructor.newInstance();
        cache.put(id, entity);
        com.fasterxml.jackson.databind.JsonNode mapper = objectMapper.readTree(fileReader);
        List<Entry> children = new ArrayList<>();
        for (Field field : clazz.getFields()) {
            Class<?> t = field.getType();
            if(t.isAssignableFrom(String.class)){
                 log.info(clazz.getSimpleName()+" Loading String : "+field.getName());
                field.set(entity, mapper.get(field.getName()).asText());
            }else if(t == int.class){
                 log.info(clazz.getSimpleName()+" Loading int : "+field.getName());
                field.set(entity, mapper.get(field.getName()).asInt());
            }else if(t.isPrimitive()){
                throw new RuntimeException("Primitive Type : " + t + " not supported yet.");
            }else{
                final String asText = mapper.get(field.getName()).asText();
               log.info("AsText: "+cache.toString());
                Object childObject = cache.get(asText);
                if(childObject == null){
                    log.warning(clazz.getSimpleName() + " Loading child: "+field.getName() + " with type "+t);
                    cache.put(id, entity);
                    cache.put(entity.toString(), entity);
                    childObject = load(t, mapper.get(field.getName()).asText());
                    field.set(entity, childObject);
                }else{
                      log.info(clazz.getSimpleName() + " Loading child from cache: "+field.getName() + " with type "+t);
                      field.set(entity, childObject);
                }
            }
          
        }

        return entity;
    }
    private class Entry{
        public Field field;
        Class<?> type;

        public Entry(Field field, Class<?> type) {
            this.field = field;
            this.type = type;
        }
        
    }
        
        public synchronized <T> T load(Class<T> clazz){
            try {
                final File file = buildPath(clazz, "Default").toFile();
                if(!file.exists())return null;
                final String id = objectMapper.readTree(file).asText();
            final T entity = load(clazz, id);
            cache.clear();
           return  entity;
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        
        public synchronized <T> void save(T entity){
            try{
                 String jsonId = null;
                final File filedefault = buildPath(entity.getClass(), "Default").toFile();
                if(filedefault.exists()){
                  jsonId = objectMapper.readTree(filedefault).asText();
                   final String paramId = getId(entity);
                  if(paramId == null || (!jsonId.equals(paramId))){
                      throw new RuntimeException(" Id "+paramId +" of Object ("+entity + ") is different than saved id "+jsonId+ ".");
                  }
                }else{
                    if(filedefault.getParentFile().exists()){
                        throw new RuntimeException("Cannot save Object ("+entity + "). "+entity.getClass().getSimpleName()+".class is sub-node of other Object.");
                    }
                    filedefault.getParentFile().mkdirs();
                    filedefault.createNewFile();
                     jsonId = generateId(entity);
                    FileOutputStream out2 = new FileOutputStream(filedefault);
                    objectMapper.writeValue(out2, jsonId);
                }
            save(entity, jsonId);

            }catch(Exception e){
                throw new RuntimeException(e);
            }
            cache.clear();
        }
        
    private String getId(Object obj) {
        if(!ids.containsKey(obj)){
           return null;
        }
         return ids.get(obj);
    }
    
    private String generateId(Object obj){
           if(ids.containsKey(obj)){
                throw new RuntimeException("Object "+obj + " does already have an id.");
             }
           String id = obj.toString();
           ids.put(obj, id);
           return id;
    }

    public void shutdown() {
        
    }

    @Override
    public <T> void save(T entity, String id) {
        log.info("Saving : "+entity.getClass().getSimpleName());
        try {
                 final File file = buildPath(entity.getClass(), id).toFile();
                if(!file.exists()){
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                }
               
                if(cache.containsKey(id)){
                     log.info(entity.getClass().getSimpleName()+"  entity is in Save Cache : "+entity.getClass().getSimpleName());
                    return;
                };
                cache.put(id, entity);
                log.warning(entity.getClass().getSimpleName()+" Saving entity : "+entity.getClass().getSimpleName());
                 var out = new FileOutputStream(file);
                toJson(out, entity);
                log.info(entity.getClass().getSimpleName()+" Saved entity : "+entity.getClass().getSimpleName());
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            } 
    }

    private <T> void toJson(FileOutputStream out, T entity) throws Exception {
         ObjectNode objectNode = objectMapper.createObjectNode();
         var children = new ArrayList<Object>();
        for (Field field : entity.getClass().getFields()) {
            Class<?> t = field.getType();
           log.info( entity.getClass().getSimpleName()+ " Found "+field.getName() + " with type "+ t);
            if(t.isAssignableFrom(String.class)){
                 log.info(entity.getClass().getSimpleName()+" Saving String : "+field.getName());
                objectNode.put(field.getName(), (String) field.get(entity));
            }else if(t == int.class){
                 log.info(entity.getClass().getSimpleName()+" Saving int : "+field.getName());
                  objectNode.put(field.getName(), (int) field.get(entity));
            }else if(t.isPrimitive()){
                throw new RuntimeException("Primitive Type : " + t + " not supported yet.");
            }else{
                var child = field.get(entity);
                if(child == null) continue;
                String childId = getId(child);
                if(childId == null) childId = generateId(child);
               objectNode.put(field.getName(), childId);
                
               if(!cache.containsKey(childId)){
                   log.warning(entity.getClass().getSimpleName()+" Saving child : "+child.getClass().getSimpleName());
                  
                  save(child,  childId);
                   
               }else{
                    log.info(entity.getClass().getSimpleName()+"  child is in Save Cache : "+child.getClass().getSimpleName());
               }
               
            }
        }
        objectMapper.writeValue(out, objectNode);
        var val = objectMapper.writeValueAsString(objectNode);
        log.info(entity.getClass().getSimpleName()+" Value: "+ val);
    }

    @Override
    public <T> void save(T entity, Long qualifier) {
          if(qualifier == null){
                save(entity);
            }else {
                save(entity, qualifier.toString());
            }
    }
        
}
