/*
 */
package com.joofthan.database;

import com.joofthan.database.models.Customer;
import com.joofthan.database.models.House;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Jonathan
 */
public class DatabaseTest {
    private static Database db = new Database("target/test"+LocalDateTime.now().getMinute() + " "+LocalDateTime.now().getSecond());


    /**
     * Test of load method, of class Database.
     */
    @Test
    public void testSaveLoadCircle() {
       var customer = new Customer("Joni", 2);
            customer.house = new House();
            customer.house.houseName = "myHouse";
            customer.house.owner = customer;
        db.save(customer);

        var res = db.load(Customer.class);

        assertEquals("Joni", res.name);
        assertEquals(2, res.alter);
        assertEquals("myHouse", res.house.houseName);
        assertEquals(res, res.house.owner);
        assertEquals(res.house, res.house.owner.house);
        
        res.house.owner.house.houseName += "bad";
        
        db.save(res);
        res = db.load(Customer.class);
       assertEquals("Joni", res.name);
        assertEquals(2, res.alter);
        assertEquals("myHousebad", res.house.houseName);
        assertEquals(res, res.house.owner);
        assertEquals(res.house, res.house.owner.house);
    }
    
}
