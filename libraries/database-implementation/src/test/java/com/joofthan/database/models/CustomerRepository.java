package com.joofthan.database.models;

import com.joofthan.database.Database;
import com.joofthan.enterprise.database.BaseRepository;


public class CustomerRepository extends BaseRepository<Customer>{
    public CustomerRepository() {
        super(new Database("myDb"), "customer");
    }
}
