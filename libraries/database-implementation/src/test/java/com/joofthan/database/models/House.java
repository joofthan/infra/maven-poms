package com.joofthan.database.models;

public class House {
public String houseName;
public Customer owner;

    public House() { }

    public House(String houseName, Customer owner) {
        this.houseName = houseName;
        this.owner = owner;
    }

}
