package com.joofthan.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.lang.syntax.elements.ClassesShouldConjunction;

import java.util.stream.Stream;

public class SimpleArchitecture {

    public static void check(String basePackage){
        check(basePackage, "");
    }
    public static void check(String basePackage, String ignore){
        check(basePackage, ignore, "");
    }

    public static void check(String basePackage, String...i){
        JavaClasses c = new ClassFileImporter().withImportOption(new ImportOption.DoNotIncludeTests()).importPackages(basePackage);

        String[] ignore = i;

        String port = "..port";
        String api = "..api";


        ArchRuleDefinition.classes()
                .that()
                .resideOutsideOfPackages(combine(new String[]{basePackage, "..interactor..", "..usecase.."}, ignore))
                .should()
                .notBePublic()
                .orShould()
                .resideInAnyPackage(port,api)
                .orShould()
                .haveSimpleNameEndingWith("Factory")
                .orShould()
                .haveSimpleNameEndingWith("Impl")
                .check(c);


        ArchRuleDefinition.classes()
                .that()
                .resideInAnyPackage(port,api)
                .should()
                .onlyDependOnClassesThat()
                .resideInAnyPackage(combine(new String[]{port,api, "java.."},ignore))
                .check(c);

        ArchRuleDefinition.classes().that()
                .resideOutsideOfPackages(port,api)
                .and()
                .resideOutsideOfPackages( combine(new String[]{"java.."}, ignore))
                .and()
                .arePublic()
                .should()
                .onlyBeAccessed()
                .byClassesThat()
                .resideInAnyPackage(basePackage,"..interactor..", "..usecase..")
                .orShould()
                .haveSimpleNameEndingWith("Factory")
                .orShould()
                .haveSimpleNameEndingWith("Impl")
                .check(c);

        ArchRuleDefinition.classes()
                .that()
                .areInterfaces()
                .and()
                .areNotAnnotations()
                .should()
                .haveSimpleNameStartingWith("I")
                .check(c);

        for (int depth = 1; depth < 10; depth++) {
            submoduleTest(basePackage, depth, ignore).check(c);
            //submoduleTest2(basePackage, depth, ignore).check(c);
        }
    }

    private static String[] combine(String[] ar1, String[] ar2){
        return Stream.of(ar1, ar2).flatMap(Stream::of).toArray(String[]::new);
    }

    private static ClassesShouldConjunction submoduleTest(String base, int depth, String[] ignore){
        String s = "";

        for (int i = 0; i < depth; i++) {
            s = s+".(*)";
        }
        return ArchRuleDefinition.noClasses()
                .that()
                .resideInAPackage(base+s+"")
                .and()
                .resideOutsideOfPackages(ignore)
                .and()
                .haveSimpleNameNotEndingWith("Factory")
                .should()
                .accessClassesThat()
                .resideInAnyPackage(base+s+".(*).(*).(*)..")
                ;
    }

    private static ClassesShouldConjunction submoduleTest2(String base, int depth, String[] ignore){
        String s = "";

        for (int i = 0; i < depth; i++) {
            s = s+".(*)";
        }
        return ArchRuleDefinition.noClasses()
                .that()
                .resideInAPackage(base+s+".*")
                .and()
                .resideOutsideOfPackages(ignore)
                .and()
                .haveSimpleNameNotEndingWith("Factory")
                .should()
                .accessClassesThat()
                .resideOutsideOfPackages(combine(new String[]{base+s+".*..", "java.."}, ignore))
                ;
    }
}
