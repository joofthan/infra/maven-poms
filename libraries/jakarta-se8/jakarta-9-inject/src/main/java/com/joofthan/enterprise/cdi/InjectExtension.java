package com.joofthan.enterprise.cdi;


import com.joofthan.cdi.CDIImpl;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestInstancePostProcessor;

import jakarta.inject.Inject;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class InjectExtension implements TestInstancePostProcessor {

    protected CDI container = new CDIImpl<>(Inject.class, System.out, "@"+ UseForInject.class.getSimpleName()+" IRepo repo = new RepoImpl() to register");

    @Override
    public void postProcessTestInstance(Object o, ExtensionContext extensionContext) throws Exception {
        System.out.println();
        EnableInject enableInject = o.getClass().getAnnotation(EnableInject.class);
        if(enableInject == null){
            enableInject = new EnableInject(){public Class<? extends Annotation> annotationType() {return null;}public Class[] mapping() {return new Class[0];}};
        }
        if(enableInject.mapping().length%2 != 0){
            throw new RuntimeException("invalid mapping. Array length is "+enableInject.mapping().length);
        }
        for (int i = 0; i < enableInject.mapping().length; i = i+2) {
            Class<?> interfaceType = enableInject.mapping()[i];
            Class<?> objType = enableInject.mapping()[i + 1];
            if(!interfaceType.isAssignableFrom(objType)){
                throw new IllegalArgumentException(objType.getName()+" is not type of "+interfaceType.getName());
            }
            System.out.println("register interfaceType \""+interfaceType+"\" as \""+objType+"\"");
            ((CDIImpl)container).registerType(interfaceType, objType);
        }

        for(Field field : o.getClass().getDeclaredFields()){
            UseForInject useForInject = field.getAnnotation(UseForInject.class);
            if(useForInject != null){
                field.setAccessible(true);
                Object obj = field.get(o);
                System.out.println("register type \""+field.getType().getName()+"\" as \""+obj.getClass().getName()+"\"");
                Class<?>[] providedTypes = useForInject.type();
                if(providedTypes.length == 0){
                    ((CDIImpl)container).registerObject(field.getType(), obj);
                }else{
                    for (int i = 0; i < providedTypes.length; i++) {
                        if(!providedTypes[0].isAssignableFrom(obj.getClass())){
                            throw new IllegalArgumentException(obj.getClass().getName()+" is not type of "+ providedTypes[0].getName());
                        }
                        ((CDIImpl)container).registerObject(providedTypes[0], obj);
                    }
                }

            }
        }
        ((CDIImpl)container).setAnnotatedFields(o);
    }
}
