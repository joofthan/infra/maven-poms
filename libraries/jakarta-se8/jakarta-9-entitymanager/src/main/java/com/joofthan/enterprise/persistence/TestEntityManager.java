package com.joofthan.enterprise.persistence;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;

import java.util.Map;

public class TestEntityManager {

    public static EntityManager create(){
        return Persistence.createEntityManagerFactory("IT")
                .createEntityManager(
                        Map.of(
                "jakarta.persistence.jdbc.url","jdbc:h2:mem:test",
                "jakarta.persistence.jdbc.password","pass",
                "jakarta.persistence.jdbc.driver","org.h2.Driver",
                "jakarta.persistence.jdbc.user","user",
                "jakarta.persistence.schema-generation.database.action","drop-and-create"
        ));
    }
}
