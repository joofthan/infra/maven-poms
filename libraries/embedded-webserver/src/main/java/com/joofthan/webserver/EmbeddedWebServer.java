

package com.joofthan.webserver;

import com.joofthan.enterprise.application.Request;
import com.joofthan.enterprise.application.RequestType;
import com.joofthan.enterprise.application.Response;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.util.function.Function;

public class EmbeddedWebServer {

    private int port;

    public EmbeddedWebServer(int port) {
        this.port = port;
    }
    
    public void run(Function<Request, Response> c) {
        try{
            HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
            output("Create Context");
            server.createContext("/", (HttpExchange exchange) -> {
                try {
                     try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter( exchange.getResponseBody())) {
                    final String uri = exchange.getRequestURI().toString();
                    final String body = new String(exchange.getRequestBody().readAllBytes());
                    RequestType method = RequestType.valueOf(exchange.getRequestMethod());
                     final Request request = new Request(uri, body, method);
                     try{
                         var res = c.apply(request);
                        exchange.sendResponseHeaders(200, res.body().length());
                        outputStreamWriter.write(res.body());
                     }catch (Exception e){
                       var message = e.getMessage();
                       exchange.sendResponseHeaders(500, message.length());
                       outputStreamWriter.write(message);
                       throw e;
                     }
                    
                }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            });
            output("Create Default Executor");
            server.setExecutor(null); // creates a default executor
            output("Start Server");
            server.start();
            output("Start Server finished");
            output("Running on: http://localhost:" + port );
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    private void output(String text){
        System.out.println(text);
    }
}
