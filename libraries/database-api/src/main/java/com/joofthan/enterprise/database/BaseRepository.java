

package com.joofthan.enterprise.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseRepository<T> {
        private IDatabase db;
        private String key;
        private RootData rootData =  new RootData();
        
        public BaseRepository(IDatabase db, String key) {
            this.key=key;
            this.db = db;
            rootData = db.load(RootData.class);
            if(rootData == null){
                rootData = new RootData();
            }
        }

        
        public List<T> findAll(){
            return new ArrayList<>(get(key));
        }
        
        public void save(T entity){
            final List<T> list = get(key);
            if(list.contains(entity)){
            }else{
                list.add(entity);
            }
           db.save(rootData);
        }
        
        public void delete(T entity){
            get(key).remove(entity);
            db.save(rootData);
        }

    List<T> get(String key){
        if(!rootData.map.containsKey(key)){
           rootData.map.put(key,  new ArrayList<>());
        }
            return (List<T>)rootData.map.get(key);
    }
    
    public static class RootData{
        public Map<String, List<Object>>map = new HashMap<>();
    }

}
