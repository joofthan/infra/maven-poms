/*
 */
package com.joofthan.enterprise.database;

/**
 *
 * @author Jonathan
 */
public interface IDatabase {
    <T> T load(Class<T> clazz);
    <T> T load(Class<T> clazz, String qualifier);
    <T> T load(Class<T> clazz, Long qualifier);
    <T> void save(T entity);
    <T> void save(T entity, String qualifier);
    <T> void save(T entity, Long qualifier);
}
