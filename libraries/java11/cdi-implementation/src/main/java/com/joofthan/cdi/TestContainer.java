package com.joofthan.cdi;

import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

@Deprecated
public class TestContainer<INJ extends Annotation> {
    public final Class<INJ> annotationClass;
    PrintStream out;

    String msg;
    Map<Class<?>, Object> map = new HashMap<>();
    Map<Class<?>, Class<?>> typeMap = new HashMap<>();
    public TestContainer(Class<INJ> an, PrintStream out, String msg){
        this.annotationClass = an;
        this.out = out;
        this.msg = msg;
    }
    public <T> T setAnnotatedFieldsOn(T obj){
        return setAnnotatedFieldsIn(obj, "");
    }
    private <T> T setAnnotatedFieldsIn(T obj, String pre){
        Class<?> theClass = obj.getClass();
        if(out != null){
            out.println(pre + "-> "  + theClass.getSimpleName());
        }
        try {
            for(Field currentField : theClass.getDeclaredFields()){
                boolean fieldIsAnnotated = currentField.getAnnotation(annotationClass) != null;
                if(fieldIsAnnotated){
                    currentField.setAccessible(true);
                    Object fieldInstance = getOrCreateInstance(currentField.getType());
                    setAnnotatedFieldsIn(fieldInstance, pre+"-");
                    currentField.set(obj, fieldInstance);
                }
            }
            return obj;
        } catch (IllegalAccessException e) {
            throw new UnsupportedOperationException(e);
        }
    }
    private <T> T getOrCreateInstance(Class<T> type){
        if(map.containsKey(type)){
            return (T)map.get(type);
        }
        try {
            if(type.isInterface()){
                throw new UnsupportedOperationException("No instance registered for \""+type.getName()+"\" use: "+msg);
            }
            Constructor<T> declaredConstructor = type.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            T obj = declaredConstructor.newInstance();
            map.put(type, obj);
            return obj;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new UnsupportedOperationException(e);
        }
    }
    public TestContainer<INJ> register(Class<?> type, Object obj){
        map.put(type, obj);
        return this;
    }

    public TestContainer<INJ> register(Class<?> type, Class<?> objType){
        map.put(type, objType);
        return this;
    }
}
