package com.joofthan.cdi;

import com.joofthan.enterprise.cdi.ApplicationScoped;
import com.joofthan.enterprise.cdi.CDI;
import com.joofthan.enterprise.cdi.InjectionException;
import com.joofthan.enterprise.cdi.RequestScoped;

import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class CDIImpl<INJ extends Annotation> implements CDI {
    boolean configurationFinished;
    private final Map<Class<?>, Class<?>> mapping = new HashMap<>();
    private final Map<Class<?>, Object> singletons = new HashMap<>();

    public final Class<INJ> annotationClass;
    PrintStream out;
    String msg;

    public CDIImpl(){
        this.annotationClass = null;
        this.out = null;
        this.msg = "";
    }

    public CDIImpl(Class<INJ> an, PrintStream out, String msg){
        this.annotationClass = an;
        this.out = out;
        this.msg = msg;
    }

    @Override
    public <T, R extends T> void register(Class<T> i, Class<R> c) {
         if(configurationFinished){
            throw new InjectionException("Configuration already finished. Configuration change not allowed after using CDI.inject().");
        }
        mapping.put(i, c);
    }

   @Override
    public <T, R extends T> void register(Class<T> i, R obj) {
        //Class<R> clazz = (Class<R>) obj.getClass();
        //register(i, clazz);
        singletons.put(i, obj);
    }


    @Override
    public <T> T inject(Class<T> interfaze) {
        return doInject(interfaze, "");
    }
    public <T> T doInject(Class<T> interfaze, String dep) {
        configurationFinished = true;
        Class<T> implClazz = interfaze;
        if(mapping.containsKey(interfaze)){
            implClazz = (Class<T>) mapping.get(interfaze);
        }
        if(implClazz.isInterface()){
            //throw new InjectionException("Mapping not found for interface \""+implClazz.getName()+"\". Register implementation using \"CDI.register()\"");
        }
        if(out != null){
            out.println(dep + "-> "  + implClazz.getName());
        }
        if(hasAnnotation(ApplicationScoped.class, interfaze, implClazz) && hasAnnotation(RequestScoped.class, interfaze, implClazz)){
            throw new InjectionException("More than One Scope Annotation present on: "+implClazz.getName());
        }
        if(hasAnnotation(ApplicationScoped.class, interfaze, implClazz)){
            return singleton(interfaze, implClazz, dep);
        }
        if(hasAnnotation(RequestScoped.class, interfaze, implClazz)){
            return newInstance(implClazz, dep);
        }

        //Fallbacks for annotation free classes.
        if(singletons.containsKey(interfaze)){
            return singleton(interfaze, implClazz, dep);
        }

        return newInstance(implClazz, dep);
    }

    private <T, A extends Annotation> boolean hasAnnotation(Class<A> annotationClass, Class<T> key, Class<T> value) {
        return key.isAnnotationPresent(annotationClass) || value.isAnnotationPresent(annotationClass);
    }

    private <T, R extends T> T singleton(Class<T> Interfaze, Class<R> implClazz, String dep)  {
        if (!singletons.containsKey(Interfaze)) {
            singletons.put(Interfaze, newInstance(implClazz, dep));
        }
        return (T) singletons.get(Interfaze);
    }

    private <T> T newInstance(Class<T> implClazz, String dep){
        try {
            T instanceWithSomeDeps = createWithConstructorDependencies(implClazz, dep);
            setAnnotatedFields(instanceWithSomeDeps,dep);
            return instanceWithSomeDeps;
        } catch (InstantiationException e) {
            throw new InjectionException("Class Injection failed on "+implClazz.getName() + ": "+e.getMessage(), e);
        } catch (IllegalAccessException e) {
            throw new InjectionException("Class Injection failed: Class or Constructor is not public on \""+implClazz.getName()+"\": "+e.getMessage(), e);
        } catch (NoSuchMethodException e) {
            throw new InjectionException("Class Injection failed: public Constructor not found on \""+implClazz.getName()+"\": "+e.getMessage(),e);
        } catch (InvocationTargetException e) {
            throw new InjectionException("Class Injection failed on \""+implClazz.getName() + "\": "+e.getMessage(),e);
        } catch (StackOverflowError e) {
            throw new InjectionException("Circular dependencies on \""+implClazz.getName() + "\": "+e.getMessage(),e);
        }
    }

    private <T> T createWithConstructorDependencies(Class<T> implClazz, String dep) throws IllegalArgumentException, SecurityException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException, InjectionException {
        Constructor<?>[] constructors = implClazz.getDeclaredConstructors();
        if(constructors.length != 1){
            throw new InjectionException("Expected one constructor on \""+implClazz.getName()+"\" but found "+constructors.length);
        }
        Constructor<?> constructor = constructors[0];
        Class<?>[] parameter = constructor.getParameterTypes();
        if(parameter.length == 0){
            Constructor<T> declaredConstructor = implClazz.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            return declaredConstructor.newInstance();
        }
        Object[] dependencies = new Object[parameter.length];
        for (int i = 0; i < parameter.length; i++) {
            dependencies[i] = doInject(parameter[i], dep+"-");
        }
        constructor.setAccessible(true);
       return (T) constructor.newInstance(dependencies);
    }


    public <T> T setAnnotatedFields(T obj){
        return setAnnotatedFields(obj, "");
    }
    private <T> T setAnnotatedFields(T obj, String dep){
        Class<?> theClass = obj.getClass();
        if(annotationClass == null){
            return obj;
        }
        try {

            for(Field currentField : theClass.getDeclaredFields()){
                boolean fieldIsAnnotated = currentField.getAnnotation(annotationClass) != null;
                if(fieldIsAnnotated){
                    currentField.setAccessible(true);
                    Object fieldInstance = doInject(currentField.getType(), dep+"-");
                    currentField.set(obj, fieldInstance);
                }
            }
            return obj;
        } catch (IllegalAccessException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void registerType(Class type, Class objType) {
        mapping.put(type, objType);
    }

    public void registerObject(Class<?> type, Object obj) {
        singletons.put(type, obj);
    }
}
