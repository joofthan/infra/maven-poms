package com.joofthan.cdi;

import com.joofthan.enterprise.cdi.CDI;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InjectTest {

    private static CDI cdi = new CDIImpl();
 
    @BeforeAll
    static void init(){
        cdi.register(MyInterface.class, MyClassAlternative.class);
    }

    @Test
    void testInject(){
        MyService myService = cdi.inject(MyService.class);
        assertEquals("hello", myService.getData());
    }

    @Test
    void testInjectInterface(){
        MyInterface my = cdi.inject(MyInterface.class);
        assertEquals("Class2", my.data());
    }


    @Test
    void subClassStructure(){
        SubClass my = cdi.inject(SubClass.class);

        assertEquals("hello", my.getMyService().getData());
    }

    public class SubClass{
        private MyService myService;
        public SubClass(MyService myService) {
            this.myService = myService;
        }

        public MyService getMyService(){
            return myService;
        }
    }


    interface MyInterface{
        String data();
    }
    public static class MyClass implements MyInterface{
        public String data(){return "Class1";}
    }
    public static class MyClassAlternative implements MyInterface{
        public String data(){return "Class2";}
    }

    public static class MyService {
        public String getData(){
            return "hello";
        }
    }
}