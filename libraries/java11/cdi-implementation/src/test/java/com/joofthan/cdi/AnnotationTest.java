package com.joofthan.cdi;

import com.joofthan.enterprise.cdi.CDI;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnnotationTest {

    private static CDI cdi;

    @BeforeAll
    static void init(){
        cdi = new CDIImpl(Autowired.class, System.out, "TODO");
    }


    @Test
    void fieldInjection(){
        MyService service= cdi.inject(MyService.class);
        assertEquals("hello", service.data());
    }

    @Retention(RetentionPolicy.RUNTIME)
    @interface Autowired{}


    class MyService{
        @Autowired
        MyRepo repo;

        String data(){
            return  repo.data();
        }
    }

    class MyRepo{
        String data(){
            return "hello";
        }
    }

}
