

package com.joofthan.cdi;

import com.joofthan.enterprise.cdi.CDI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ArgumentConstructorTest {

    private static CDI cdi = new CDIImpl();
    
    @BeforeAll
    static void init(){
            cdi.register(DataRepo.class, new MyRepo2("from repo2"));
    }
    
    @Test
    void argumentConstructor(){
        MyService service = cdi.inject(MyService.class);
        Assertions.assertEquals("Repo: from repo2", service.info());
    }
    
    
    static class MyService{
        MyRepo1 repo1;
        DataRepo dataRepo;
        MyService(MyRepo1 repo1, DataRepo dataRepo) {
            this.repo1 = repo1;
            this.dataRepo = dataRepo;
        }
        String info(){
            return dataRepo.data();
        }
        
    }
    
    static interface DataRepo{
        String data();
    }
    static class MyRepo1{}
    static class MyRepo2 implements DataRepo{

        private final String data;

        public MyRepo2(String data) {
            this.data = data;
        }
        
        public String data(){
            return "Repo: "+data;
        }
    }
}
