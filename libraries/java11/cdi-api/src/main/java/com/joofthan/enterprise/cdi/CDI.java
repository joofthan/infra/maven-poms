package com.joofthan.enterprise.cdi;

public interface CDI {
    <INTERFACE, TYPE extends INTERFACE> void register(Class<INTERFACE> i, Class<TYPE> c);
    <INTERFACE, INST_OBJ extends INTERFACE> void register(Class<INTERFACE> i, INST_OBJ c);
    <TYPE> TYPE inject(Class<TYPE> c);
}
