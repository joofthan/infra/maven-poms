
import com.joofthan.enterprise.application.Request;
import com.joofthan.enterprise.application.RequestType;
import com.joofthan.nano.NanoApp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



public class MainTest {
    @Test
    void helloTest(){
        var nano = new NanoApp();
        //nano.register(new RequestConverter(), );
        var app = nano.process(new Request("/hello", "{\"myValue\":12}", RequestType.GET));
        app.get("/hello", () -> "Response");
        app.get("/hellos", () -> "lkasjkdasd");
        
        Assertions.assertEquals("Response", app.getResponse());
    }
}
