package com.joofthan.nano;

import com.joofthan.enterprise.application.RequestApp;
import com.joofthan.enterprise.application.RequestListener;
import com.joofthan.enterprise.application.Response;
import java.util.concurrent.Callable;
import java.util.function.Function;

class SubModuleRequest implements RequestApp{

    private final RequestApp wrapped;
    private final String path;

    public SubModuleRequest(RequestApp requestApp, String path) {
        wrapped = requestApp;
        this.path = path;
    }

    
    @Override
    public <R> void get(String pattern, Callable<R> c) {
        wrapped.get(path + pattern, c);
    }

    @Override
    public <T> void get(String pattern, Function<String, T> c) {
       wrapped.get(path + pattern, c);
    }

    @Override
    public <R> void post(String pattern, Callable<R> c) {
        wrapped.post(path + pattern, c);
    }

    @Override
    public <T> void post(String pattern, Function<String, T> c) {
        wrapped.post(path + pattern, c);
    }

    @Override
    public void executeModule(RequestListener listener) {
       wrapped.executeModule(this.path, listener);
    }

    @Override
    public void executeModule(String path, RequestListener listener) {
        wrapped.executeModule(this.path + path, listener);
    }

    @Override
    public Response getResponse() {
           return wrapped.getResponse();
    }

}
