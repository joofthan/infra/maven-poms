package com.joofthan.nano;

import com.joofthan.enterprise.application.*;
import java.util.concurrent.Callable;
import java.util.function.Function;

public abstract class GenericRequestApp implements RequestApp {
        
     @Override
    public <R> void get(String pattern, Callable<R> c) {
         processRequest(RequestType.GET, pattern, (e)-> {
             try {
                 return c.call();
             } catch (Exception ex) {
                 throw new RuntimeException(ex);
             }
         });
    }
    
    @Override
    public <R> void get(String pattern, Function<String, R> c){
        processRequest(RequestType.GET, pattern, c);
    }

    @Override
    public <R> void post(String pattern, Callable<R> c) {
         processRequest(RequestType.POST, pattern, (e)-> {
             try {
                 return c.call();
             } catch (Exception ex) {
                 throw new RuntimeException(ex);
             }
         });
    }
    
    @Override
    public <R>  void post(String pattern, Function<String, R> c){
        processRequest(RequestType.POST, pattern, c);
    }

    protected abstract <R> void processRequest(RequestType type, String pattern, Function<String, R> c);


}
