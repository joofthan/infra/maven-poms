package com.joofthan.nano;

import com.joofthan.enterprise.application.*;
import java.util.Map;
import java.util.function.Function;

class NanoRequestApp extends GenericRequestApp {

    private final String url;
    private boolean processed = false;
    private Object response = null;
    private final Request request;
    private Map<Class<?>, ResponseConverter> responseConverters;

    NanoRequestApp(Request request, Map<Class<?>, ResponseConverter> responseConverters) {
        this.request = request;
        this.url = this.request.url();
        this.responseConverters = responseConverters;
    }

    @Override
    protected <T> void processRequest(RequestType type, String pattern, Function<String, T> c) {
         if(request.type() == type && url.matches(pattern)){
             if(processed) throw new RuntimeException("The current request matches more than one pattern.");
            response = c.apply(request.body());
            processed = true;
        }
    }

    @Override
    public void executeModule(RequestListener listener) {
         listener.process(this);
    }

    @Override
    public void executeModule(String path, RequestListener listener) {
        if(url.startsWith(path)){
            listener.process(new SubModuleRequest(this, path));
        }
    }


    @Override
    public Response getResponse() {
        if(!processed){
            final String message = "No mapping found for : "+url;
            output(message);
            return new Response(message, ContentType.TEXT);
        }
        return convert(response);
    }

    private <T> Response convert(T object) {
        if(responseConverters.containsKey(object.getClass())){
            return responseConverters.get(object.getClass()).convert(object);
        }
        
        for (var entry : responseConverters.entrySet()) {
            if(entry.getKey().isAssignableFrom(object.getClass())){
                return entry.getValue().convert(object);
            }
        }
        
        throw new RuntimeException("No ResponseConverter found for type: "+object.getClass() +". "+responseConverters);
    }

    private void output(String message) {
          System.out.println(message);
    }

}
