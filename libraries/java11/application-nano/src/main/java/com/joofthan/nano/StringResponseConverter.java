package com.joofthan.nano;

import com.joofthan.enterprise.application.*;

public class StringResponseConverter implements ResponseConverter{

    @Override
    public Response convert(Object object) {
        return Response.text((String) object);
    }

}
