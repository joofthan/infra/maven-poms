package com.joofthan.nano;

import com.joofthan.enterprise.application.App;
import com.joofthan.enterprise.application.Request;
import com.joofthan.enterprise.application.RequestApp;
import com.joofthan.enterprise.application.RequestListener;
import com.joofthan.enterprise.application.ResponseConverter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NanoApp implements App{

    private List<RequestListener> modules = new ArrayList<>();
    private Map<Class<?>, ResponseConverter> responseConverters = new HashMap<>();

    public NanoApp(RequestListener...listeners) {
        for (RequestListener listener : listeners) {
            modules.add(listener);
        }
        responseConverters.put(String.class, new StringResponseConverter());
    }

    @Override
    public RequestApp process(Request req) {
        final NanoRequestApp app = new NanoRequestApp(req, responseConverters);
        modules.forEach(module -> app.executeModule(module));
        return app;
    }

    @Override
    public void addPlugin(RequestListener requestListener) {
        modules.add(requestListener);
    }

    @Override
    public void addPlugin(Class<?> type, ResponseConverter responseConverter) {
        if(responseConverters.containsKey(type)){
            throw new RuntimeException("Converter for type \""+type+"\" already registered.");
        }
       responseConverters.put(type, responseConverter);
    }

}
