package com.joofthan.enterprise.application;

public interface IContentType {
    String mimeType();
}
