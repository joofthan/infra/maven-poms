package com.joofthan.enterprise.application;

public interface RequestListener extends Plugin{
    void process(RequestApp app);
}