package com.joofthan.enterprise.application;

public interface ResponseConverter extends Plugin {
    Response convert(Object object);
}

