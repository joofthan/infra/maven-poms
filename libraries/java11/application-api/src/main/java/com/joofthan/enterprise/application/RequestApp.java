package com.joofthan.enterprise.application;

import java.util.concurrent.Callable;
import java.util.function.Function;

public interface RequestApp {
    <R> void get(String pattern, Callable<R> c);
    <R> void get(String pattern, Function<String, R> c);
    <R> void post(String pattern, Callable<R> c);
    <R> void post(String pattern, Function<String, R> c);
    public void executeModule(RequestListener listener);
    public void executeModule(String path, RequestListener listener);
    Response getResponse();
}
