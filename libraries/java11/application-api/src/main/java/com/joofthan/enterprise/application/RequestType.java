package com.joofthan.enterprise.application;

public enum RequestType {
    GET, POST, PUT, DELETE, PATCH
}