

package com.joofthan.enterprise.application;

public enum ContentType implements IContentType{
    TEXT("text/plain"), HTML("text/html"), JSON("application/json"), XML("application/xml"), PNG("image/png")
    ;
    private String mimeType;

    private ContentType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String mimeType() {
        return mimeType;
    }
        
}
