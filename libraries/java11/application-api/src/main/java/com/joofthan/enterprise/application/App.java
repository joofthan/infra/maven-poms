

package com.joofthan.enterprise.application;

public interface App {
    void addPlugin(RequestListener requestListener) ;
    void addPlugin(Class<?> type, ResponseConverter responseConverter);
    RequestApp process(Request req);
}
