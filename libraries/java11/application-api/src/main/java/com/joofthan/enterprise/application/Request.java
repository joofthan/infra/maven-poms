package com.joofthan.enterprise.application;

import java.util.Objects;

public final class Request {
    private final String url;
    private final String body;
    private final RequestType type;

    public Request(String url, String body, RequestType type) {
        this.url = url;
        this.body = body;
        this.type = type;
    }

    public String url() {
        return url;
    }

    public String body() {
        return body;
    }

    public RequestType type() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Request) obj;
        return Objects.equals(this.url, that.url) &&
                Objects.equals(this.body, that.body) &&
                Objects.equals(this.type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, body, type);
    }

    @Override
    public String toString() {
        return "Request[" +
                "url=" + url + ", " +
                "body=" + body + ", " +
                "type=" + type + ']';
    }
}