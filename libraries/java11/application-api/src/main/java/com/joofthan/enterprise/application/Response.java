package com.joofthan.enterprise.application;

import java.util.Objects;

public final class Response {
    private final String body;
    private final IContentType contentType;

    public Response(String body, IContentType contentType) {
        this.body = body;
        this.contentType = contentType;
    }

    public String body() {
        return body;
    }

    public IContentType contentType() {
        return contentType;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Response) obj;
        return Objects.equals(this.body, that.body) &&
                Objects.equals(this.contentType, that.contentType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(body, contentType);
    }

    @Override
    public String toString() {
        return "Response[" +
                "body=" + body + ", " +
                "contentType=" + contentType + ']';
    }

    public static Response json(String body) {
        return new Response(body, ContentType.JSON);
    }

    public static Response text(String body) {
        return new Response(body, ContentType.TEXT);
    }
}