

package com.joofthan.enterprise.annotation.writer;

import java.util.ArrayList;
import java.util.List;

public class MethodWriter {
    
    private String methodName;
    private String returnType;
    private List<String> parameter = new ArrayList<>();
    private String lines="";
    
    public MethodWriter toConstructor(){
        this.returnType = "";
        return this;
    }
    
     public MethodWriter(String methodName) {
        this.methodName = methodName;
        this.returnType = "void";
    }
    public MethodWriter(String methodName, String returnType) {
        this.methodName = methodName;
        this.returnType = returnType;
    }

    public MethodWriter parameter(List<String> parameter) {
        this.parameter = parameter;
        return this;
    }
    
    public MethodCallWriter addMethodCall(){
        MethodCallWriter m = new MethodCallWriter();
        lines += ""+m.generateString();
        return m;
    }
    
    public MethodWriter addLine(String text){
        lines += "        "+ text+"\n";
        return this;
    }
    
    public String generateMethod(){
        return "    public  "+returnType+" "+methodName+"("+parameter()+"){\n"
                +lines
                 +"    }\n" ;
    }
    
        public String generateStaticMethod(){
        return "    public static "+returnType+" "+methodName+"("+parameter()+"){\n"
                +lines
                + "    }\n" ;
    }
    
    
    private String parameter(){
        String result = "";
        for (int i = 0; i < parameter.size(); i++) {
            if(i > 0){
                result += ", ";
            }
            result += parameter.get(i) + " p"+i;
        }
        return result;
    }

}
