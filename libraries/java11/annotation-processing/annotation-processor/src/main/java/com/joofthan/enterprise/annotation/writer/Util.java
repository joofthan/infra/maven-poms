package com.joofthan.enterprise.annotation.writer;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;

public class Util {

    public static void writeClass(ProcessingEnvironment processingEnv, ClassWriter writer) {
        writeClass(processingEnv, writer.fullClassName(), writer.generateContent());
    }

    public static void writeClass(ProcessingEnvironment processingEnv, String fullName, String content) {
        try {
            JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(fullName);
            PrintWriter out = new PrintWriter(builderFile.openWriter());
            out.write(content);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static TypeElement classOf(Element element) {
        if(!(element.getEnclosingElement() instanceof TypeElement)){
            throw new RuntimeException(element+" does not have Enclosing Element of type TypeElement. It is: "+element.getEnclosingElement());
        }

        return (TypeElement) element.getEnclosingElement();
    }
}
