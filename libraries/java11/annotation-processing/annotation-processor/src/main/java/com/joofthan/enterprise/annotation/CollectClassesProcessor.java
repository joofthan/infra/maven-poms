/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Cloudogu GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package com.joofthan.enterprise.annotation;


import com.joofthan.enterprise.annotation.writer.ClassWriter;
import com.joofthan.enterprise.annotation.writer.MethodWriter;
import com.joofthan.enterprise.annotation.writer.Util;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.Arrays;
import java.util.Set;

import static com.joofthan.enterprise.annotation.writer.Util.classOf;
import static com.joofthan.enterprise.annotation.writer.Util.writeClass;

@SupportedAnnotationTypes("com.joofthan.enterprise.annotation.CollectClasses")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class CollectClassesProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotationType, RoundEnvironment roundEnv) {
        out("");
        out("####################Called process with: "+ Arrays.toString(annotationType.toArray())+"#######################");
        for (TypeElement generateClassAnnotationType : annotationType) {
            Set<? extends Element> customAnnotationTypes = roundEnv.getElementsAnnotatedWith(generateClassAnnotationType);
            for (Element customAnnotationType:customAnnotationTypes) {
                try{
                    processSubAnnotation((TypeElement) customAnnotationType, roundEnv);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

        }

        return false;
    }

    private void out(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,msg);
    }

    private void error(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,msg);
        throw new RuntimeException(msg);
    }

    private void processSubAnnotation(TypeElement customAnnotation, RoundEnvironment roundEnv) {
        CollectClasses value = customAnnotation.getAnnotation(CollectClasses.class);
        final ClassWriter classWriter = new ClassWriter(value.className())
                                                                         .packageName(processingEnv.getElementUtils().getPackageOf(customAnnotation).toString()+".generated");
        String listType = "java.lang.Class";
        MethodWriter method = new MethodWriter(value.methodName(), "java.util.List<"+listType+">");
        method.addLine("java.util.List<"+listType+"> resultList = new java.util.ArrayList<>();");
        for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(customAnnotation)) {
            if(annotatedElement instanceof TypeElement){
                out("found @"+customAnnotation+" at " + annotatedElement);
                method.addLine("resultList.add("+annotatedElement+".class);");
            }else if(annotatedElement instanceof ExecutableElement){
                out("found @"+customAnnotation+" at " + Util.classOf(annotatedElement)+"#"+annotatedElement);
                method.addLine("resultList.add("+Util.classOf(annotatedElement)+".class);");
            }else{
                error(customAnnotation + " (@"+CollectClasses.class.getSimpleName()+") is only allowed on ElementType.TYPE (Class) or ElementType.METHOD (Function). But was on: "+classOf(annotatedElement)+ "#"+ annotatedElement);
            }
        }
        method.addLine("return resultList;");
        classWriter.addStaticMethod(method);
        out("");
        out("Generating Class: "+ classWriter.getClassName()+ "\n"+classWriter.generateContent());
        writeClass(processingEnv, classWriter);
    }

}
