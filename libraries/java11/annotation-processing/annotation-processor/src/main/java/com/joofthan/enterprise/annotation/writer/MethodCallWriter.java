package com.joofthan.enterprise.annotation.writer;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MethodCallWriter {
    private String className;
    private String methodName;
    private List<String> parameter = new ArrayList<>();

    public MethodCallWriter callMethod(String methodName) {
        this.methodName = methodName.replaceAll("\\(","").replaceAll("\\)", "");;
        return this;
    }

    public MethodCallWriter packageAndClass(String className) {
        this.className = className;
        return this;
    }

    public String generateString(){
        return "        "+className+"."+methodName+"("+ parameter() +");\n";
    }

    private String parameter() {
        return IntStream.range(0, parameter.size()).boxed().map(e -> "p" + e).collect(Collectors.joining(", "));
    }

    public MethodCallWriter parameter(List<String> parameter) {
        this.parameter = parameter;
        return this;
    }
}
