/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Cloudogu GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package com.joofthan.enterprise.annotation;

import com.joofthan.enterprise.annotation.writer.ClassWriter;
import com.joofthan.enterprise.annotation.writer.MethodWriter;

import java.util.Set;
import javax.annotation.processing.*;
import javax.lang.model.element.*;
import javax.lang.model.SourceVersion;

import static com.joofthan.enterprise.annotation.writer.Util.classOf;
import static com.joofthan.enterprise.annotation.writer.Util.writeClass;

@SupportedAnnotationTypes("com.joofthan.enterprise.annotation.Initialize")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class InitializeProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
         for (TypeElement annotation : annotations) {
             MethodWriter methodWriter = new MethodWriter("initializeAll");
            for (Element method : roundEnv.getElementsAnnotatedWith(annotation)) {
                methodWriter.addMethodCall()
                            .callMethod(method.getSimpleName().toString())
                            .packageAndClass(classOf(method).getQualifiedName().toString());
            }
             writeClass(processingEnv, new ClassWriter("ClassInitializer")
                                                .packageName(getClass().getPackage().getName())
                                                .addStaticMethod(methodWriter));
         }


        return false;
    }
}
