package com.joofthan.enterprise.annotation.writer;

public class ClassWriter {
    private String packageName;
    private String className;
 
    private String lines = "";


    public ClassWriter(String className) {
        this.className = className;
    }
    
    private ClassWriter addLine(String text){
        lines += "  "+text+"\n";
        return this;
    }

    public String generateContent(){
        return header() +
                lines +
                footer();
    }

    private String footer() {
        return "}";
    }

    private String header() {
        return "package " + packageName + ";\n\npublic class "+className+" {\n\n" ;
    }

    public String fullClassName() {
        return packageName+"."+className;
    }

    public ClassWriter packageName(String packageName) {
        this.packageName = packageName;
        return this;
    }


    public String getClassName() {
        return className;
    }

    public ClassWriter addMethod(MethodWriter parameter) {
        lines += parameter.generateMethod();
        return this;
    }
    
        public ClassWriter addStaticMethod(MethodWriter parameter) {
        lines += parameter.generateStaticMethod();
        return this;
    }
}
