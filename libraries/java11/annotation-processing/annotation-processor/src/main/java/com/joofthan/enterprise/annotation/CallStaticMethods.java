package com.joofthan.enterprise.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 
 * Provides an convinient interface for creating CustomAnnotations.
 * 
 */
@Target(value={ANNOTATION_TYPE, TYPE})
@Retention(value=RUNTIME)
public @interface CallStaticMethods {
    String name();
    String method();
    String[] parameter() default {};
}
