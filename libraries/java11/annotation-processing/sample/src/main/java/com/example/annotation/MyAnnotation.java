

package com.example.annotation;

import com.joofthan.enterprise.annotation.CollectClasses;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@CollectClasses(className = "RegelFinder", methodName = "findAllRegeln")
@Target(value={TYPE, METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {}
