package com.example.annotation;


import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import com.example.annotation.generated.RegelFinder;
import java.util.List;

public class Hello {

    public static void main(String[] args) {
        System.out.println("Start");
        List<Class>li =  RegelFinder.findAllRegeln();
        for (Class class1 : li) {
            if (class1.isAnnotationPresent(MyAnnotation.class)) {
                 System.out.println("Class-Annotation found: "+class1.getName());
            }
           Method[] methods = class1.getDeclaredMethods();
            for (Method method : methods) {
                System.out.print("Method-Annotation found: "+class1.getName()+"#"+method.getName() + "(");
                for (Parameter parameter : method.getParameters()) {
                    System.out.print(parameter.getType() + " "+parameter.getName());
                }
                System.out.println(")");
            }
        }
        System.out.println(li.size());
    }
}