package com.joofthan.enterprise.json;

import java.nio.charset.StandardCharsets;

class Content {
    public final String type;
    public byte[] content;

    private Content(String type, String text) {
        this.type = type;
        this.content = text.getBytes(StandardCharsets.UTF_8);
    }

    Content(String type, byte[] content) {
        this.type = type;
        this.content = content;
    }

    Content(Content content1, Content content2) {
        if(content1 == null || content2 == null || !content1.type.equals(content2.type)){
            throw new IllegalArgumentException();
        }
        this.type = content1.type;
        this.content = new byte[content1.content.length+content2.content.length];
        content = content1.content;
        for (int i = 0; i < content2.content.length; i++) {
            content[i+content1.content.length] = content2.content[i];
        }
    }

    public static Content combine(Content c1, Content c2){
        return new Content(c1,c2);
    }
}
