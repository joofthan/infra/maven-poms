package com.joofthan.enterprise.json;


import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JSON extends Content {

    public static final String S = "\"";
    private final Map<String,String> map = new HashMap<>();

    public JSON() {
        super("text/json;charset=UTF-8", new byte[0]);
    }

    private JSON(JSON other) {
        super(other.type, other.content);
        this.map.putAll(other.map);

    }

    public JSON with(String key, String value) {
        JSON copy = new JSON(this);
        copy.map.put(format(key), format(value));
        copy.content = copy.asBytes();
        return copy;
    }

    public JSON with(String key, boolean value) {
        JSON copy = new JSON(this);
        copy.map.put(format(key), String.valueOf(value));
        copy.content = copy.asBytes();
        return copy;
    }

    public JSON with(String key, int value) {
        JSON copy = new JSON(this);
        copy.map.put(format(key), String.valueOf(value));
        copy.content = copy.asBytes();
        return copy;
    }

    public JSON with(String key, double value) {
        JSON copy = new JSON(this);
        copy.map.put(format(key), String.valueOf(value));
        copy.content = copy.asBytes();
        return copy;
    }

    public JSON with(String key, JSON subJson) {
        JSON copy = new JSON(this);
        copy.map.put(format(key), subJson.asString().replaceAll("\\n", "\n  "));
        copy.content = copy.asBytes();
        return copy;
    }

    public JSON with(String key, List<String> value) {
        JSON copy = new JSON(this);
        copy.map.put(format(key),"[ "+ value.stream().map(v -> format(v)).collect(Collectors.joining(", "))+" ]");
        copy.content = copy.asBytes();
        return copy;
    }

    private String format(String value) {
        return S + (
                value.replaceAll("\n", "")
                .replaceAll("\\\"", "\\\\\"")
                .replaceAll("\\?","\\\\?")
               ) + S;
    }


    public byte[] asBytes() {
        return asString().getBytes(StandardCharsets.UTF_8);
    }

    private String asString() {
        String json = "{\n";
        json += map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(e-> "  "+e.getKey()+": " + e.getValue())
                .collect(Collectors.joining(",\n"));

        return (json + "\n}");
    }

    @Override
    public String toString() {
        return asString();
    }
}
