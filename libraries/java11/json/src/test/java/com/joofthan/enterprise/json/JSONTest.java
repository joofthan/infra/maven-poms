package com.joofthan.enterprise.json;

import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class JSONTest {

    private JSON json;

    @BeforeEach
    void setUp() {
        json = new JSON();
    }

    @Test
    void immutable() {
        json.with("key", "value");
        assertEquals("{\n\n}", json.asBytes());
    }

    @Test
    void withReplace() {
        json = json.with("key", "va\"l\"ue");
        String expected = "{\n\"key\": \"va\\\"l\\\"ue\"\n}";
        assertEquals(expected, json.asBytes());
    }


    @Test
    void withOneValue() {
        json = json.with("key", "value");
        String expected = "{\n\"key\": \"value\"\n}";
        assertEquals(expected, json.content);
    }

    @Test
    void withTwoValue() {
        json = json.with("one", "value1").with("two", "value2");
        String expected = "{\n\"one\": \"value1\",\n\"two\": \"value2\"\n}";
        assertEquals(expected, json.content);
    }

    @Test
    void withSubJson() {
        json = json.with("one", "value1").with("jason", new JSON().with("name", "burn"));
        String expected = "{\n\"jason\": {\n" +
                            "\"name\": \"burn\"" +
                            "\n},\n" +
                            "\"one\": \"value1\"\n}";
        assertEquals(expected, json.content);
    }

    @Test
    void withArray() {
        json = json.with("list", Arrays.asList("hallo", "wer", "bin", "ich"));
        String expected = "{\n\"list\": [ \"hallo\", \"wer\", \"bin\", \"ich\" ]\n}";
        assertEquals(expected, json.content);
    }
}
